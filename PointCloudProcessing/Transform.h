/**
 * @file   Transform.h
 * @Author Muhammet Emin Kurt email=mekurt55@gmail.com
 * @date   December 2021
 @brief transform s�n�f�n�n tan�m�d�r.
 *
*/
#pragma once
#include "PointCloud.h"
#include "Point.h"
#include <iomanip>
#include<math.h>
#include<Eigen/Dense>
#include<Eigen/Geometry>
using namespace std;
class Transform
{
	Eigen::Vector3d Angles;
	Eigen::Vector3d Trans;
	Eigen::Matrix4d Transmatrix;

public:
	Transform();
	~Transform();
	void Setrotation(Eigen::Vector3d);
	void Settranslation(Eigen::Vector3d);
	Point Dotransform(Point p);
	PointCloud Dotransform(PointCloud pc);
	Eigen::Vector3d Getangles();
	Eigen::Vector3d Gettrans();
	void Setangles(Eigen::Vector3d);
	void Settrans(Eigen::Vector3d);

	list<Point> Dotransform(list<Point>);

	Eigen::Matrix4d Getrottest();
	void Setrottest(Eigen::Matrix4d);

	Eigen::Matrix4d Gettranslationtest();
	void Settranslationtest(Eigen::Matrix4d);
};

